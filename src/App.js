import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="toolbar">
        <header className="toolbar__header">
          <h1 className="toolbar__title">Welcome to React</h1>
        </header>
        <p className="toolbar__progress">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
